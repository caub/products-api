const { pool, uid } = require('../util');

// todo allow orderBy, order other than description ASC
exports.get = ({ ids, q, first = 20, after } = {}) => {

	let ps = ids ? Map(ids.map(id => [id, pool.products.get(id)])) : pool.products;

	ps = ps.sort((a, b) => a.description.localeCompare(b.deccription));

	if (q) {
		// todo filter with text query (mongo full text)
	}
	if (after) {
		// filter where id > id or [field] !== value (id, field and value taken from pagination token)
	}

	return ps.slice(0, first);
};


exports.merge = async o => {
	if (!Object.keys(o || {})) {
		throw new Error('Nothing to add/update');
	}

	let p = {};
	if (o.id) {
		p = pool.products.get(o.id);
		p.updated = new Date();
	} else {
		p.id = uid(); // hopefully no conflict (just for test)
		p.created = new Date();
	}

	const newP = Object.assign(p, o);
	pool.products = pool.products.set(p.id, newP);
	return newP;
};

exports.delete = id => {
	const n = +pool.products.has(id);
	pool.products = pool.products.delete(id);
	return { n };
};
