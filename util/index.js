const crypto = require('crypto');
const { Map } = require('immutable');

/**
 * Generate some unique id
 * @param {*} len 
 */
const uid = (len = 12) => crypto.randomBytes(len).toString('base64')
	.replace(/=+$/, '')
	.replace(/\+/g, '-')
	.replace(/\//g, '_');

exports.uid = uid;

/**
 * Fake/mock storage for now, should use mongo soon
 */
exports.pool = {
	products: Map(Array.from({ length: 10 }, (_, i) => {
		const id = uid();
		const cost = Math.floor(1e4 * Math.random()) / 100; // cost in USD
		return [id, {
			id,
			description: `Product ${i + 1}`,
			cost,
			price: Math.floor(cost + 2 * Math.random()),
			stock: Math.floor(8 * Math.random())
		}];
	}))
};

exports.exchangeRates = (price, currency = 'USD') => {
	const rate = currency === 'USD' ? 1 : (1 - 0.1 * Math.random());
	return price * rate;
};
