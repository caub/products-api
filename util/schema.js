const {
	GraphQLObjectType: ObjectType,
	GraphQLInputObjectType: InputObjectType,
	GraphQLNonNull: NonNull,
	GraphQLID: ID,
	GraphQLInt: Int,
	GraphQLFloat: Float,
	GraphQLString: String,
	GraphQLList: List,
	GraphQLSchema: Schema,
	GraphQLEnumType: Enum
} = require('graphql');

const Product = require('../models/Product');
const { exchangeRates } = require('../util');

const Currency = new Enum({
	name: 'Currency',
	values: {
		USD: { value: 0 },
		EUR: { value: 1 },
	}
});


const listArgs = {
	first: {
		type: Int
	},
	after: {
		type: String
	},
	q: {
		type: String
	},
};

const getByIdArgs = {
	id: {
		type: new NonNull(ID)
	},
};

const currencyArgs = {
	currency: {
		type: Currency
	}
};


const ProductType = new ObjectType({
	name: 'Product',
	fields: {
		id: { type: new NonNull(ID) },
		description: { type: new NonNull(String) },
		cost: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(p.cost, currency)
		},
		price: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(p.price, currency)
		},
		stock: { type: Int },
		created: { type: new NonNull(Float) },
		updated: { type: Float },
		totalCost: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(p.stock * p.cost, currency)
		},
		totalPrice: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(p.stock * p.price, currency)
		},
	}
});
const ProductInput = new InputObjectType({
	name: 'ProductInput',
	fields: {
		id: { type: ID },
		description: { type: String },
		cost: { type: Int },
		price: { type: Int },
		stock: { type: Int },
	}
});

const QueryType = new ObjectType({
	name: 'Query',
	description: 'The root of all queries',
	fields: {
		rand: {
			type: Float,
			args: { n: { type: Int } },
			resolve: (_, { n = 1 }) => n * Math.random()
		},

		products: {
			type: new NonNull(new List(new NonNull(ProductType))),
			args: listArgs,
			resolve: (_, args) => Product.get(args)
		},
		product: {
			type: new NonNull(ProductType),
			args: getByIdArgs,
			resolve: (_, { id }) => Product.get({ ids: [id] }),
		},

		totalCost: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(
				Product.get().reduce((a, p) => a + p.stock * p.cost, 0),
				currency
			)
		},
		totalPrice: {
			type: Float,
			args: currencyArgs,
			resolve: (p, { currency = 'USD' }) => exchangeRates(
				Product.get().reduce((a, p) => a + p.stock * p.price, 0),
				currency
			)
		}
	}
});

const DeletionResult = new ObjectType({
	name: 'DeletionResult',
	fields: {
		n: { type: Int }
	}
});

const MutationType = new ObjectType({
	name: 'Mutation',
	description: 'The root of all mutations',
	fields: {
		mergeProduct: {
			type: ProductType,
			args: { input: { type: new NonNull(ProductInput) } },
			resolve: (_, { input }) => Product.merge(input)
		},
		deleteProduct: {
			type: DeletionResult,
			args: { id: { type: new NonNull(ID) } },
			resolve: (_, { id }) => Product.delete(id)
		},
	}
});

module.exports = new Schema({
	query: QueryType,
	mutation: MutationType,
});
