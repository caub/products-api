const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./util/schema');
const cors = require('./util/cors');

const app = express().disable('x-powered-by');

if (process.env.NODE_ENV !== 'production') {
	app.set('json spaces', 2);
}

app.use(cors);

app.use('/gql', graphqlHTTP({
	schema,
	graphiql: true
}));

app.get('/', (req, res) => res.send('Show product table here'));

let server;
/**
 * start app
 * @param port (optional, defaults to process.env.PORT || 3000)
 * @returns server
 */
const start = (port = process.env.PORT || 3000) => {
	server = app.listen(port);
	return server;
};

module.exports = {
	start,
	close() {
		server.close();
	}
};

if (!module.parent) { // if this module is directly invoked, start the server
	const server = start();
	console.log('listening:', server.address().port);
}
