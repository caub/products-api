## GraphQL API for generic products

- start with `npm run start` or `npm run dev`
- browse http://localhost:3000/gql
- try queries like:

```graphql
{
  products(first: 2){
    id
    price(currency: EUR)
    stock
    totalPrice(currency: EUR)
  }
  totalCost(currency: EUR)
}
```
or
```graphql
query MyProducts($cur: Currency){
  products(first: 2){
    id
    price(currency: $cur)
    stock
    totalPrice(currency: $cur)
  }
  totalCost(currency: $cur)
}
```
variables: 
```json
{"cur": "EUR"}
```

response: 

```json
{
  "data": {
    "products": [
      {
        "id": "Lqt2EwZ6yoKZlXXY",
        "price": 75.5720607822434,
        "stock": 6,
        "totalPrice": 425.0872031440331
      },
      {
        "id": "w6oZ8jxA3yIPnm4G",
        "price": 93.10356487764308,
        "stock": 0,
        "totalPrice": 0
      }
    ],
    "totalCost": 2136.3759838648507
  }
}
```